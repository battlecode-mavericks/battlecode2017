package mavericks3;

import battlecode.common.*;

import java.util.Random;

/**
 * Created by schueppert on 1/12/2017.
 */
public class RobotPlayer {
    static RobotController rc;
    static MapLocation mapCenter;
    static RobotInfo[] nearbyEnemy;
    static BodyInfo[] nearbyBullets;
    static RobotInfo nearestEnemy;
    static Direction d = Direction.getNorth();
    static Random rand;
    static int[] unitCount = new int[RobotType.values().length];
    static int masterArchon;
    static boolean hasMoved = false;

    public static void run(RobotController r) throws GameActionException {
        rc = r;
        rand = new Random((long) rc.getID());
        mapCenter();
        if (rc.getLocation() == rc.getInitialArchonLocations(rc.getTeam())[0]) {
            masterArchon = rc.getID();
            rc.broadcast(1, masterArchon);
        } else {
            masterArchon = rc.readBroadcast(1);
        }
        while (true) {
            int incrementChannel = 2;
            int valueChannel = 8;
            int count = rc.readBroadcast(incrementChannel + rc.getType().ordinal());
            count++;
            rc.broadcast(incrementChannel + rc.getType().ordinal(), count);
            for (int i = 0; i < RobotType.values().length; i++) {
                unitCount[i] = rc.readBroadcast(valueChannel + i);
            }
            if (rc.getID() == masterArchon) {
                for (int i = 0; i < RobotType.values().length; i++) {
                    count = rc.readBroadcast(incrementChannel + i);
                    rc.broadcast(incrementChannel + i, 0);
                    rc.broadcast(valueChannel + i, count);
                }
            }
            nearbyEnemy = rc.senseNearbyRobots(rc.getType().sensorRadius, rc.getTeam().opponent());
            nearbyBullets = rc.senseNearbyBullets();
            receive();
            // Cache Invalidation
            if (rc.canSenseLocation(nearestEnemy.location) & rc.senseNearbyRobots(nearestEnemy.ID) == null) {
                nearestEnemy = new RobotInfo(0, rc.getTeam().opponent(), RobotType.ARCHON, rc.getInitialArchonLocations(rc.getTeam().opponent())[0], 0, 0, 0);
                send(0, nearestEnemy);
            }
            if (nearbyEnemy.length > 0) {
                nearestEnemy = nearbyEnemy[0];
                for (RobotInfo e : nearbyEnemy) {
                    if (e.type == RobotType.GARDENER) nearestEnemy = e;
                    if (rc.getLocation().distanceTo(e.location) < rc.getLocation().distanceTo(nearestEnemy.location))
                        nearestEnemy = e;

                }
                send(0, nearestEnemy);
            }
            switch (rc.getType()) {
                case ARCHON:
                    archon();
                    break;

                case GARDENER:
                    gardener();
                    break;

                default:
                    unit();
            }
            rc.setIndicatorDot(nearestEnemy.location, 255, 0, 0);
            int bytecodes = Clock.getBytecodeNum();
            Clock.yield();
        }
    }

    static void runAway() throws GameActionException {
        nearestEnemy = nearbyEnemy[0];
        d = rc.getLocation().directionTo(nearestEnemy.location).opposite();
        while (true) {
            if (rc.canMove(d)) {
                rc.move(d);
                break;

            } else d = d.rotateRightRads((float) Math.PI / 8);
        }
    }

    static void dodge() {
        BulletInfo nearestBullet = (BulletInfo) nearbyBullets[0];
        for (BodyInfo b : nearbyBullets) {
            if (rc.getLocation().distanceTo(b.getLocation()) < rc.getLocation().distanceTo(nearestBullet.getLocation())) {
                nearestBullet = (BulletInfo) b;
            }
        }
        MapLocation futurePosition = nearestBullet.getLocation().add(nearestBullet.getDir(), nearestBullet.getSpeed());
        d = rc.getLocation().directionTo(futurePosition).opposite();
    }


    static void shoot() throws GameActionException {
        if (rc.canFireSingleShot() & rc.getLocation().distanceTo(nearestEnemy.location) < rc.getType().sensorRadius) {
            rc.fireSingleShot(rc.getLocation().directionTo(nearestEnemy.location));
        }
        TreeInfo[] trees = rc.senseNearbyTrees(rc.getType().strideRadius);
        if (trees != null && trees.length > 0) {
            for (TreeInfo tree : trees) {
                if (tree.getTeam() == rc.getTeam().opponent() & rc.canFireSingleShot())
                    rc.fireSingleShot(rc.getLocation().directionTo(tree.location));
            }
        }
    }

    static void send(int channel, RobotInfo bot) throws GameActionException {
        int data = 0;
        MapLocation adjustedLocation = bot.location.translate(-mapCenter.x, -mapCenter.y);
        int x = (int) adjustedLocation.x % 64 + 64;
        int y = (int) adjustedLocation.y % 64 + 64;
        int type = bot.getType().ordinal() % 8;
        int health = (int) bot.getHealth() / 50 % 8;
        int id = bot.ID % 2048;
        data = x + y * 128 + type * 128 * 128 + health * 128 * 128 * 8 + id * 128 * 128 * 8 * 8;
        rc.broadcast(channel, data);
    }

    static void receive() throws GameActionException {
        int data = rc.readBroadcast(0);
        if (data != 0) {
            int x = data % 128;
            data = (int) (data - x) / 128;
            int y = data % 128;
            data = (int) (data - y) / 128;
            int type = data % 8;
            data = (int) (data - type) / 8;
            int health = data % 8;
            data = (int) (data - health) / 8;
            health = health * 50;
            int id = data;
            MapLocation location = new MapLocation((float) x - 64, y - 64).translate(mapCenter.x, mapCenter.y);
            nearestEnemy = new RobotInfo(id, rc.getTeam().opponent(), RobotType.values()[type], location, (float) health, 0, 0);
        }
    }

    static void mapCenter() throws GameActionException {
        MapLocation[] myArchons = rc.getInitialArchonLocations(rc.getTeam());
        MapLocation[] enemyArchons = rc.getInitialArchonLocations(rc.getTeam().opponent());
        float x = 0;
        float y = 0;
        for (int i = 0; i < myArchons.length; i++) {
            x += myArchons[i].x + enemyArchons[i].x;
            y += myArchons[i].y + enemyArchons[i].y;
        }
        mapCenter = new MapLocation(x / myArchons.length / 2, y / myArchons.length / 2);
        nearestEnemy = new RobotInfo(0, rc.getTeam().opponent(), RobotType.ARCHON, enemyArchons[0], 0, 0, 0);
        send(0, nearestEnemy);
    }

    static void archon() throws GameActionException {
        if (nearbyEnemy != null & nearbyEnemy.length > 0) runAway();
        else if (nearbyBullets != null && nearbyBullets.length > 0) dodge();
        else if (unitCount[RobotType.GARDENER.ordinal()] < unitCount[RobotType.ARCHON.ordinal()]) {
            for (float i = 0; i < 2 * Math.PI; i += Math.PI / 8) {
                d = new Direction(i);
                if (rc.canHireGardener(d)) {
                    rc.hireGardener(d);
                }
            }
        }
    }

    static void gardener() throws GameActionException {
        if (nearbyEnemy != null & nearbyEnemy.length > 0) runAway();
        else if (nearbyBullets != null && nearbyBullets.length > 0) dodge();
        else {
            TreeInfo[] trees = rc.senseNearbyTrees(rc.getType().strideRadius);
            for (float i = 0; i < 2 * Math.PI; i += Math.PI / 8) {
                d = new Direction(i);
                if (false) {
                    if (rc.canBuildRobot(RobotType.SCOUT, d)) {
                        rc.buildRobot(RobotType.SCOUT, d);
                    }
                } else {
                    if (rc.canBuildRobot(RobotType.SOLDIER, d)) {
                        rc.buildRobot(RobotType.SOLDIER, d);
                    }
                }
                if (rc.canPlantTree(d) & !hasMoved & false) {
                    rc.plantTree(d);
                }
                for (TreeInfo tree : trees) {
                    if (rc.canWater(tree.ID)) rc.water(tree.ID);
                }
            }
        }
    }

    static void unit() throws GameActionException {
        d = rc.getLocation().directionTo(nearestEnemy.location);
        shoot();  // Always shoot
        TreeInfo[] trees = rc.senseNearbyTrees(rc.getType().strideRadius);
        if (trees != null && trees.length > 0) {
            for (TreeInfo tree : trees) {
                if (rc.canShake(tree.ID)) rc.shake(tree.ID);
                if (rc.canChop(tree.ID)) rc.chop(tree.ID);
            }
        }
        if (nearbyBullets != null && nearbyBullets.length > 0) dodge();
        if (rc.canMove(d)) rc.move(d);
        else {
            double offset = rand.nextFloat() * 2 * Math.PI;
            for (float i = 0; i < 2 * Math.PI; i += Math.PI / 8) {
                d = new Direction(i + (float) offset);
                if (rc.canMove(d)) {
                    rc.move(d);
                    hasMoved = true;
                    break;
                }
            }
        }
        shoot(); //Shoot if you haven't already!

    }
}