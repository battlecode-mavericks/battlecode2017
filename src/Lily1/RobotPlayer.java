package Lily1;

import battlecode.common.*;


/**
 * Created by Michael on 1/15/17.
 */
public class RobotPlayer {
    static RobotController rc;
    static MapLocation mapCenter;

    public void run(RobotController r) throws GameActionException {
        rc = r;
        switch (rc.getType()) {
            case ARCHON:
                runArchon();
                break;

            case GARDENER:
                runGardener();
                break;

            case LUMBERJACK:
                runLumberjack();
                break;
        }
    }

    void runArchon() throws GameActionException {
        float d;
        for (d = 0; d < 2 * Math.PI; d += Math.PI / 4) {
            if (rc.canHireGardener(new Direction(d))) rc.hireGardener(new Direction(d));
        }
    }

    void runGardener() {

    }

    void runLumberjack() {

    }
}

