package danielbot;
import battlecode.common.*;

public strictfp class RobotPlayer {
static RobotController rc;

/**
 * run() is the method that is called when a robot is instantiated in the Battlecode world.
 * If this method returns, the robot dies!
**/
@SuppressWarnings("unused")
public static void run(RobotController rc) throws GameActionException {

    // This is the RobotController object. You use it to perform actions from this robot,
    // and to get information on its current status.
    RobotPlayer.rc = rc;

    // Here, we've separated the controls into a different method for each RobotType.
    // You can add the missing ones or rewrite this into your own control structure.
        switch (rc.getType()) {
        case ARCHON:
            runArchon();
            break;
        case GARDENER:
            runGardener();
            break;
        case SOLDIER:
            runSoldier();
            break;
        case LUMBERJACK:
            runLumberjack();
            break;
        case SCOUT:
            runScout();
    }
}

static void runArchon() throws GameActionException {
    System.out.println("I'm an archon!");
    int gardenerCount = 0;
    Direction d = new Direction(0);
    MapLocation startingLocation = rc.getLocation();
    // The code you want your robot to perform every round should be in this loop
    while (true) {
        
        // Try/catch blocks stop unhandled exceptions, which cause your robot to explode
        try {
            MapLocation myLocation = rc.getLocation();
            
            if (rc.canMove(d) == false){
                d = randomDirection();
            }

            if (gardenerCount < 1 && rc.canHireGardener(d)){
                rc.hireGardener(d);
                gardenerCount ++;
            }
            // Randomly attempt to build a gardener in this direction
            if (rc.canHireGardener(d) && Math.random() < .05) {
                rc.hireGardener(d);
            
            }
            
            BulletInfo[] bullets = new BulletInfo[50];
            bullets = rc.senseNearbyBullets();
            for (int i = 0; i < bullets.length; i++){
                if (willCollideWithMe(bullets[i])){
                    d = myLocation.directionTo(bullets[i].getLocation());
                    d = d.rotateLeftDegrees(90);
                    break;
                       
                }
            }

            // Broadcast archon's location for other robots on the team to know
            rc.broadcast(0,(int)myLocation.x);
            rc.broadcast(1,(int)myLocation.y);
            
            if (myLocation.distanceTo(startingLocation) > 10){
                d = myLocation.directionTo(startingLocation);
            }
            tryMove(d);

            // Clock.yield() makes the robot wait until the next turn, then it will perform this loop again
            Clock.yield();

        } catch (Exception e) {
            System.out.println("Archon Exception");
            e.printStackTrace();
        }
    }
}

static void runGardener() throws GameActionException {
    System.out.println("I'm a gardener!");
    Direction d = new Direction(0);
    // The code you want your robot to perform every round should be in this loop
    while (true) {
        MapLocation myLocation = rc.getLocation();
        boolean allowWater = false;
        double randomBuild = Math.random();
        // Try/catch blocks stop unhandled exceptions, which cause your robot to explode
        try {

            // Listen for home archon's location
            int xPos = rc.readBroadcast(0);
            int yPos = rc.readBroadcast(1);
            MapLocation archonLoc = new MapLocation(xPos,yPos);
            Direction buildD = randomDirection();
            // Randomly attempt to build a soldier or lumberjack in this direction
            if (rc.canBuildRobot(RobotType.SOLDIER, buildD) && randomBuild < .6) {
                rc.buildRobot(RobotType.SOLDIER, buildD);
            } 
            if (rc.canBuildRobot(RobotType.LUMBERJACK, buildD) && randomBuild < 1 && randomBuild > 0.6 && rc.isBuildReady()) {
                rc.buildRobot(RobotType.LUMBERJACK, buildD);
            }
            TreeInfo[] sensedTrees = new TreeInfo[10];
            sensedTrees = rc.senseNearbyTrees(2);
            if (rc.canShake() && sensedTrees.length > 0) {
                if (sensedTrees[0].getContainedBullets() > 3){
                    rc.shake(sensedTrees[0].getID());
                    allowWater = true;
                }
            }
            if (rc.canWater() && sensedTrees.length > 0 && allowWater == true) {
                rc.water(sensedTrees[0].getID());
            }
            
            // Move randomly
            BulletInfo[] bullets = new BulletInfo[50];
            bullets = rc.senseNearbyBullets();
            for (int i = 0; i < bullets.length; i++){
                if (willCollideWithMe(bullets[i])){
                    d = myLocation.directionTo(bullets[i].getLocation());
                    d = d.rotateLeftDegrees(90);
                    break;
                       
                }
            }
            tryMove(d);
            if (myLocation.distanceTo(archonLoc) > 10){
                d = myLocation.directionTo(archonLoc);
            }
            if (rc.canMove(d) == false){
                d = randomDirection();
            }

            // Clock.yield() makes the robot wait until the next turn, then it will perform this loop again
            Clock.yield();

        } catch (Exception e) {
            System.out.println("Gardener Exception");
            e.printStackTrace();
        }
    }
}

static void runSoldier() throws GameActionException {
    System.out.println("I'm an soldier!");
    Team enemy = rc.getTeam().opponent();
    Direction d = new Direction(0);
    int dodgeDirection = 0;
    int turnAlternate = -1;
    MapLocation oldLocation = new MapLocation(0,0);
    // The code you want your robot to perform every round should be in this loop
    while (true) {
        int moving = 1;

        if (Math.random() < 0.5){
            dodgeDirection = 0;
        }
        else{
            dodgeDirection = 1;
        }
        Direction enemyDirection = new Direction(0);
        // Try/catch blocks stop unhandled exceptions, which cause your robot to explode
        try {
            MapLocation myLocation = rc.getLocation();
            MapLocation enemyLocation = new MapLocation(rc.readBroadcast(2), rc.readBroadcast(3));

            // See if there are any nearby enemy robots
            RobotInfo[] robots = rc.senseNearbyRobots(-1, enemy);
            TreeInfo[] sensedTrees = new TreeInfo[10];
            sensedTrees = rc.senseNearbyTrees(2);

            // If there are some...
            if (robots.length > 0) {
                if (turnAlternate == -1){
                    oldLocation = robots[0].location;
                    turnAlternate = 0;
                }
                // And we have enough bullets, and haven't attacked yet this turn...
                if (rc.canFireSingleShot()) {
                    // ...Then fire a bullet in the direction of the enemy.
                    if (turnAlternate == 1){
                        enemyDirection = oldLocation.directionTo(robots[0].location);
                        oldLocation = robots[0].location;
                        rc.fireSingleShot(rc.getLocation().directionTo(robots[0].location.add(enemyDirection, (myLocation.distanceTo(robots[0].location)/2) + 4)));
                    }

                    moving = 2;
                }
                if (enemyLocation.x == 0 && enemyLocation.y == 0){
                    rc.broadcast(2,(int)robots[0].getLocation().x);
                    rc.broadcast(3,(int)robots[0].getLocation().y);
                }
            }
            if (myLocation.distanceTo(enemyLocation) <= 10 && robots.length == 0)
            {
                rc.broadcast(2,0);
                rc.broadcast(3,0);
            }

            if (rc.canShake() && sensedTrees.length > 0) {
                if (sensedTrees[0].getContainedBullets() > 3){
                    rc.shake(sensedTrees[0].getID());
                }
            }
            // Move 
            if (moving == 1){
                tryMove(d);
            }
            if (enemyLocation.x != 0 || enemyLocation.y != 0) {
                if (myLocation.distanceTo(enemyLocation) > 8){
                    d = myLocation.directionTo(enemyLocation);    
                }     
            }
            else if (myLocation.distanceTo(enemyLocation) < 5){
                d = d.opposite();
            }
            else if (myLocation.distanceTo(enemyLocation) > 5 && myLocation.distanceTo(enemyLocation) < 8){
                moving = 0;
            }
            
            BulletInfo[] bullets = new BulletInfo[50];
            bullets = rc.senseNearbyBullets();
            for (int i = 0; i < bullets.length; i++){
                if (willCollideWithMe(bullets[i])){
                    d = myLocation.directionTo(bullets[i].getLocation());
                    d = d.rotateLeftDegrees(90);
                    if (moving == 2) {
                        tryMove(d);
                    }
                    break;
                       
                }
            }

            if (rc.canMove(d) == false){
                d = randomDirection();
            }
            
            turnAlternate = 1;
            // Clock.yield() makes the robot wait until the next turn, then it will perform this loop again
            Clock.yield();

            } catch (Exception e) {
                System.out.println("Soldier Exception");
                e.printStackTrace();
            }
        }
    }

    static void runLumberjack() throws GameActionException {
        System.out.println("I'm a lumberjack!");
        Team enemy = rc.getTeam().opponent();
        Direction d = new Direction(0);
        TreeInfo[] sensedTrees = new TreeInfo[10];
        // The code you want your robot to perform every round should be in this loop
        while (true) {
            int moving = 1;
            sensedTrees = rc.senseNearbyTrees(-1);

            // Try/catch blocks stop unhandled exceptions, which cause your robot to explode
            try {
                MapLocation enemyLocation = new MapLocation(rc.readBroadcast(2), rc.readBroadcast(3));
                // See if there are any enemy robots within striking range (distance 1 from lumberjack's radius)
                RobotInfo[] robots = rc.senseNearbyRobots(RobotType.LUMBERJACK.bodyRadius+GameConstants.LUMBERJACK_STRIKE_RADIUS, enemy);
                RobotInfo[] longRobots = rc.senseNearbyRobots(-1, enemy);
                MapLocation myLocation = rc.getLocation();
                if(robots.length > 0 && !rc.hasAttacked()) {
                    // Use strike() to hit all nearby robots!
                    rc.strike();
                    rc.broadcast(2,(int)robots[0].getLocation().x);
                    rc.broadcast(3,(int)robots[0].getLocation().y);
                }
                if (myLocation.distanceTo(enemyLocation) <= 10 && longRobots.length == 0)
                {
                    rc.broadcast(2,0);
                    rc.broadcast(3,0);
                }
                
                else {
                    // No close robots, so search for robots within sight radius
                    robots = rc.senseNearbyRobots(-1,enemy);

                    /*if (sensedTrees.length > 0){
                        myLocation = rc.getLocation();
                        MapLocation treeLocation = sensedTrees[0].getLocation();
                        Direction toTree = myLocation.directionTo(treeLocation);

                        tryMove(toTree);
                    }*/

                    /*if (sensedTrees.length > 0) {
                        if (rc.canChop(sensedTrees[0].getID())){
                            rc.chop(sensedTrees[0].getID());
                            moving = 0;
                        }
                    }*/
                    BulletInfo[] bullets = new BulletInfo[50];
                    bullets = rc.senseNearbyBullets();
                    for (int i = 0; i < bullets.length; i++){
                        if (willCollideWithMe(bullets[i])){
                            d = myLocation.directionTo(bullets[i].getLocation());
                            d = d.rotateLeftDegrees(60);
                            break;
                               
                        }
                    }
                    if (moving == 1){
                        tryMove(d);
                    }
                    if (enemyLocation.x != 0 || enemyLocation.y != 0) {
                        d = myLocation.directionTo(enemyLocation);    
                    }
                    if (rc.canMove(d) == false){
                        if (sensedTrees.length > 0){
                            if (rc.canChop(sensedTrees[0].getID())){
                                    rc.chop(sensedTrees[0].getID());
                            }
                            else {
                                d = randomDirection();
                            }
                        }
                        else {
                            d = randomDirection();
                        }
                    }
                }

                // Clock.yield() makes the robot wait until the next turn, then it will perform this loop again
                Clock.yield();

            } catch (Exception e) {
                System.out.println("Lumberjack Exception");
                e.printStackTrace();
            }
        }
    }

    static void runScout() throws GameActionException {
        System.out.println("I'm a scout!");
        Team enemy = rc.getTeam().opponent();
        Direction d = new Direction(0);
        // The code you want your robot to perform every round should be in this loop
        while (true) {
            int moving = 1;
            // Try/catch blocks stop unhandled exceptions, which cause your robot to explode
            try {
                MapLocation myLocation = rc.getLocation();
                MapLocation enemyLocation = new MapLocation(rc.readBroadcast(2), rc.readBroadcast(3));

                // See if there are any nearby enemy robots
                RobotInfo[] robots = rc.senseNearbyRobots(-1, enemy);

                // If there are some...
                if (robots.length > 0) {
                    rc.broadcast(2,(int)robots[0].getLocation().x);
                    rc.broadcast(3,(int)robots[0].getLocation().y);
                }
                if (myLocation.distanceTo(enemyLocation) <= 10 && robots.length == 0)
                {
                    rc.broadcast(2,0);
                    rc.broadcast(3,0);
                }
                // Move 
                if (moving == 1){
                    tryMove(d);
                }
                if (myLocation.distanceTo(enemyLocation) < 5){
                    d = d.opposite();
                }
                
                if (rc.canMove(d) == false){
                    d = randomDirection();
                }

                // Clock.yield() makes the robot wait until the next turn, then it will perform this loop again
                Clock.yield();

                } catch (Exception e) {
                    System.out.println("Scout Exception");
                    e.printStackTrace();
                }
            }
        }



    /**
     * Returns a random Direction
     * @return a random Direction
     */
    static Direction randomDirection() {
        return new Direction((float)Math.random() * 2 * (float)Math.PI);
    }

    /**
     * Attempts to move in a given direction, while avoiding small obstacles directly in the path.
     *
     * @param dir The intended direction of movement
     * @return true if a move was performed
     * @throws GameActionException
     */
    static boolean tryMove(Direction dir) throws GameActionException {
        return tryMove(dir,20,3);
    }

    /**
     * Attempts to move in a given direction, while avoiding small obstacles direction in the path.
     *
     * @param dir The intended direction of movement
     * @param degreeOffset Spacing between checked directions (degrees)
     * @param checksPerSide Number of extra directions checked on each side, if intended direction was unavailable
     * @return true if a move was performed
     * @throws GameActionException
     */
    static boolean tryMove(Direction dir, float degreeOffset, int checksPerSide) throws GameActionException {

        // First, try intended direction
        if (rc.canMove(dir)) {
            rc.move(dir);
            return true;
        }

        // Now try a bunch of similar angles
        boolean moved = false;
        int currentCheck = 1;

        while(currentCheck<=checksPerSide) {
            // Try the offset of the left side
            if(rc.canMove(dir.rotateLeftDegrees(degreeOffset*currentCheck))) {
                rc.move(dir.rotateLeftDegrees(degreeOffset*currentCheck));
                return true;
            }
            // Try the offset on the right side
            if(rc.canMove(dir.rotateRightDegrees(degreeOffset*currentCheck))) {
                rc.move(dir.rotateRightDegrees(degreeOffset*currentCheck));
                return true;
            }
            // No move performed, try slightly further
            currentCheck++;
        }

        // A move never happened, so return false.
        return false;
    }

    /**
     * A slightly more complicated example function, this returns true if the given bullet is on a collision
     * course with the current robot. Doesn't take into account objects between the bullet and this robot.
     *
     * @param bullet The bullet in question
     * @return True if the line of the bullet's path intersects with this robot's current position.
     */
    static boolean willCollideWithMe(BulletInfo bullet) {
        MapLocation myLocation = rc.getLocation();

        // Get relevant bullet information
        Direction propagationDirection = bullet.dir;
        MapLocation bulletLocation = bullet.location;

        // Calculate bullet relations to this robot
        Direction directionToRobot = bulletLocation.directionTo(myLocation);
        float distToRobot = bulletLocation.distanceTo(myLocation);
        float theta = propagationDirection.radiansBetween(directionToRobot);

        // If theta > 90 degrees, then the bullet is traveling away from us and we can break early
        if (Math.abs(theta) > Math.PI/2) {
            return false;
        }

        // distToRobot is our hypotenuse, theta is our angle, and we want to know this length of the opposite leg.
        // This is the distance of a line that goes from myLocation and intersects perpendicularly with propagationDirection.
        // This corresponds to the smallest radius circle centered at our location that would intersect with the
        // line that is the path of the bullet.
        float perpendicularDist = (float)Math.abs(distToRobot * Math.sin(theta)); // soh cah toa :)

        return (perpendicularDist <= rc.getType().bodyRadius);
    }
    
}
