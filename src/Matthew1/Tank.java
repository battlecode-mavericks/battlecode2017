package Matthew1;

import battlecode.common.*;

import java.util.Random;

public class Tank extends Robot {
    Tank(RobotController r) {
        super(r);
        while (true) {
            try {
                startLoop();
                everyLoop();
                endLoop();
            } catch (GameActionException g) {
                System.out.println("I had an error!  " + g);
                rc.setIndicatorDot(rc.getLocation(), 255, 255, 0);
                Clock.yield();
            }
        }
    }
    void everyLoop() throws GameActionException {
        // This is the stuff that happens every round
        //avoidBullets();
        if (sortedNearbyRobots.length > 0) {
            chaseEnemies(RobotPlayer.FIRE_RANGE_TANK);
            shouldWander = false;
        }
        else {
            shouldWander = true;
        }
    }
}