package Matthew1;

import battlecode.common.*;

public abstract class Robot {
    RobotController rc;
    RobotInfo[] sortedNearbyRobots; // What the robot sees + received data, sorted
    RobotInfo[] nearbyVisibleEnemyRobots; // What the robot SEES
    RobotInfo supposedNearestRobot; // Received data
    boolean info;
    Direction wanderDirection;
    boolean shouldWander;
    boolean retreat;
    boolean shootObstructions;
    //MapLocation loc;
    Robot(RobotController r) {
        rc = r;
        shouldWander = true;
        shootObstructions = false;
        wanderDirection = new Direction(rad(RobotPlayer.random.nextInt(360)));
        System.out.println("Hello!  I am a robot of ID " + rc.getID() + ".  I am at (" + rc.getLocation().x + ", " + rc.getLocation().y + ".  ");
        //loc = rc.getLocation();
    }
    void send(RobotInfo robot) throws GameActionException {
        // We get 32 bits to send
        if (robot.getID() == -1) {rc.broadcast(rc.getRoundNum(), 0); return;}
        int type = robot.getType().ordinal(); // use RobotType robot = RobotType.values()[type] to get back to type (3 bits)
        int health = (int)(31.0 * robot.health / robot.getType().maxHealth); // int from 0-31, a % of health on unit (5 bits)
        int team;
        if (!rc.getTeam().equals(robot.getTeam())) { // Enemy
            team = 1;
        }
        else {
            team = 0;
        }
        int id = robot.getID() % 2048; // int from 0-2047 (11 bits)
        int y = (int)(31.0 * (robot.getLocation().y - RobotPlayer.mapCenter.y) / 50.0); // int from -31 to 31 (5 bits for number, 1 bit for negative)
        int yNegative = 0;
        if (y < 0) {
            y = -y;
            yNegative = 1;
        }
        int x = (int)(31.0 * (robot.getLocation().x - RobotPlayer.mapCenter.x) / 50.0); // int from -31 to 31 (5 bits for number, 1 bit for negative)
        int xNegative = 0;
        if (x < 0) {
            x = -x;
            xNegative = 1;
        }
        int data = (xNegative << 31) + (x << 26) + (yNegative << 25) + (y << 20) + (id << 9) + (team << 8) + (health << 3) + type;
        System.out.println("type: " + type + ", health: " + health + ", team: " + team +  ", id: " + id + ", y: " + (yNegative == 1 ? -y : y) + ", x: " + (xNegative == 1 ? -x : x));
        System.out.println("Sending data: " + data + " on channel " + rc.getRoundNum());
        /*int extraRounds = 0;
        switch (robot.getType()) {
            case ARCHON:
                extraRounds = 10;
                break;
            case TANK:
                extraRounds = 8;
                break;
            case SOLDIER:
                extraRounds = 4;
                break;
            case GARDENER:
                extraRounds = 12;
                break;
            case LUMBERJACK:
                extraRounds = 2;
                break;
            case SCOUT:
                extraRounds = 3;
                break;
        }
        for (int i = 0; i <= extraRounds; i++) {
            */rc.broadcast(/*i + */rc.getRoundNum(), data);
        //}
    }
    RobotInfo receive() throws GameActionException {
        int data = rc.readBroadcast(rc.getRoundNum());
        if (data == 0) {
            info = false;
            return new RobotInfo(-1, rc.getTeam().opponent(), RobotType.ARCHON, new MapLocation(0,0), -1, -1, -1);
        }
        int type = data & 7;
        RobotType robotType = RobotType.values()[type];

        double health = (data >> 3 & 31) * robotType.maxHealth / 31.0;
        int team = data >> 5 & 1;
        int id = (data >> 9 & 2047);
        double y = (data >> 20 & 31) * 50.0 / 31.0 * ((data >> 25 & 1) == 1 ? -1 : 1) + RobotPlayer.mapCenter.y;
        double x = (data >> 26 & 31) * 50.0 / 31.0 * ((data >> 31 & 1) == 1 ? -1 : 1) + RobotPlayer.mapCenter.x;
        Team teamTeam;
        if (team == 1) {
           teamTeam = rc.getTeam().opponent();
        }
        else {
            teamTeam = rc.getTeam();
        }
        System.out.println("Receiving!  type: " + type + ", health: " + health + ", team: " + team +  ", id: " + id + ", y: " + y + ", x: " + x);
        System.out.println("Received: " + data + " on channel " + rc.getRoundNum());
        return new RobotInfo(id, teamTeam, robotType, new MapLocation((float)x, (float)y), (float)health, 0,0);
    }
    RobotInfo[] getSortedNearbyRobots() throws GameActionException {
        supposedNearestRobot = receive();
        if (info && supposedNearestRobot.team.equals(rc.getTeam())) {
            retreat = true;
        }
        else {
            retreat = false;
        }
        nearbyVisibleEnemyRobots = rc.senseNearbyRobots(-1, rc.getTeam().opponent());
        //System.out.println("I sense " + nearbyVisibleEnemyRobots.length + " robots near me.");
        RobotInfo[] allNearbyRobots = new RobotInfo[nearbyVisibleEnemyRobots.length + (info ? 1 : 0)];
        System.arraycopy(nearbyVisibleEnemyRobots, 0, allNearbyRobots, 0, nearbyVisibleEnemyRobots.length);
        if (info) allNearbyRobots[allNearbyRobots.length - 1] = supposedNearestRobot;
        return sortNearbyRobots(rc.getLocation(), allNearbyRobots);
    }
    RobotInfo[] sortNearbyRobots(MapLocation currentLocation, RobotInfo[] robots) {
        for (int i = robots.length - 1; i > 0; i--) {
            if (currentLocation.distanceTo(robots[i].location) > currentLocation.distanceTo(robots[i - 1].location)) {
                RobotInfo tempRobot = robots[i];
                robots[i] = robots[i - 1];
                robots[i - 1] = tempRobot;
            }
        }
        for (int i = 0; i < robots.length - 1; i++) { // Delete all duplicates (which are next to eachother because its sorted)
            if (robots[i].getID() == robots[i+1].getID()) {
                RobotInfo[] n = new RobotInfo[robots.length - 1];
                System.arraycopy(robots, 0, n, 0, i);
                System.arraycopy(robots, i+1, n, i, robots.length - i - 1);
                robots = n;
            }
        }
        return robots;
    }
    void avoidBullets() throws GameActionException {
        if (rc.hasMoved()) return;
        BulletInfo[] nearbyBullets = rc.senseNearbyBullets();
        if (nearbyBullets != null && nearbyBullets.length > 0) {
            BulletInfo nearestBullet = nearbyBullets[0];
            for (BulletInfo b : nearbyBullets) {
                if (rc.getLocation().distanceTo(b.getLocation()) < rc.getLocation().distanceTo(nearestBullet.getLocation())) {
                    nearestBullet = b;
                }
            }
            avoid(nearestBullet.location);
        }
    }
    void avoidEnemies() throws GameActionException {
        if (rc.hasMoved()) return;
        for (RobotInfo sortedNearbyRobot : nearbyVisibleEnemyRobots) {
            if (sortedNearbyRobot.getTeam().equals(rc.getTeam().opponent())) {
                avoid(sortedNearbyRobot.location);
                if (rc.hasMoved()) break;
            }
        }
    }
    void avoid(MapLocation avoidance) throws GameActionException { // Positive offset makes the robot OK with moving slightly towards the avoidance, negative makes it move further back
        if (rc.hasMoved()) return;
        Direction[] testDirections = new Direction[2];
        testDirections[0] = new Direction(avoidance, rc.getLocation()).rotateLeftDegrees(45);
        testDirections[1] = new Direction(avoidance, rc.getLocation()).rotateRightDegrees(45);
        if (testMovements(avoidance, testDirections)) return;
        testDirections[0] = testDirections[0].rotateRightDegrees((float)(45.0 / 2.0));
        testDirections[1] = testDirections[1].rotateLeftDegrees((float)(45.0 / 2.0));
        if (testMovements(avoidance, testDirections)) return;
        testDirections[0] = testDirections[0].rotateLeftDegrees(45);
        testDirections[1] = testDirections[1].rotateRightDegrees(45);
        if (testMovements(avoidance, testDirections)) return;
        testDirections = new Direction[3];
        testDirections[0] = new Direction(avoidance, rc.getLocation());
        testDirections[1] = new Direction(avoidance, rc.getLocation()).rotateLeftDegrees(90);
        testDirections[2] = new Direction(avoidance, rc.getLocation()).rotateRightDegrees(90);
        if (testMovements(avoidance, testDirections)) return;
        /*Direction testDirection;
        for (double i = 0 - ; i < 180 + offset; i+=5) { // Try every 5 degrees to run!
            testDirection = new Direction(rad(dir.getAngleDegrees() + 90.0 - i));
            System.out.println("Testing moving " + testDirection.getAngleDegrees() + " degrees...");
            if (rc.canMove(testDirection)) {
                rc.move(testDirection);
                System.out.println("Success!");
                break;
            }
        }*/
    }
    void chaseEnemies(double shootDistance) throws GameActionException {
        tryMove(rc.getLocation().directionTo(sortedNearbyRobots[0].location), 120, 10);
        if (rc.getLocation().distanceTo(sortedNearbyRobots[0].location) < shootDistance) {
            shoot(rc.getLocation().directionTo(sortedNearbyRobots[0].location));
        }
    }
    void tryMove(Direction dir, double offset, double precision) throws GameActionException {
        if (rc.hasMoved()) return;
        for (float i = 0; i < offset; i+=precision) {
            if (rc.canMove(dir.rotateLeftDegrees(i))) {
                rc.move(dir.rotateLeftDegrees(i));
                return;
            }
            if (rc.canMove(dir.rotateRightDegrees(i))) {
                rc.move(dir.rotateRightDegrees(i));
                return;
            }
        }
        shoot(dir); // If it never was able to move its probably a tree in the way, shoot it!
    }
    boolean testMovements(MapLocation scary, Direction[] testDirections) throws GameActionException {
        double longestDistances[] = new double[testDirections.length];
        int longestDistancesLoc[] = new int[testDirections.length];
        for (int i = 0; i < testDirections.length; i++) {
            longestDistances[i] = scary.distanceTo(rc.getLocation().add(testDirections[i], rc.getType().strideRadius));
            longestDistancesLoc[i] = i;
        }
        for (int i = testDirections.length - 1; i > 0; i--) {
            if (longestDistances[i] > longestDistances[i-1]) { // Longest will be at [0]
                double tempDist = longestDistances[i];
                int tempDistLoc = longestDistancesLoc[i];
                longestDistances[i] = longestDistances[i-1];
                longestDistancesLoc[i] = longestDistancesLoc[i-1];
                longestDistances[i-1] = tempDist;
                longestDistancesLoc[i-1] = tempDistLoc;
            }
        }
        Direction testDir;
        for (int i = 0; i < testDirections.length; i++) {
            testDir = testDirections[longestDistancesLoc[i]];
            if (rc.canMove(testDir)) {
                rc.move(testDir);
                return true;
            }
        }
        return false;
    }
    void shakeTrees() throws GameActionException {
        TreeInfo[] trees = rc.senseNearbyTrees();
        if (!rc.canShake()) return;
        for (TreeInfo tree : trees) {
            if (rc.canShake(tree.ID)) {
                rc.shake(tree.ID);
            }
            if (rc.canWater()) {
                if (rc.canWater(tree.ID)) {
                    rc.water(tree.ID);
                }
            }
        }
    }
    void move() throws GameActionException {
        if (rc.hasMoved()) return;
        while (Clock.getBytecodesLeft() > 500) {
            if (rc.canMove(wanderDirection)) {
                rc.move(wanderDirection);
                break;
            } else {
                if (shootObstructions && rc.isLocationOccupiedByTree(rc.getLocation().add(wanderDirection))) { // If there's a tree in the way and its set to shoot
                    shoot(wanderDirection);
                }
                else {
                    wanderDirection = new Direction(rad(RobotPlayer.random.nextInt(360)));
                }
            }
        }
    }
    void shoot(Direction dir) throws GameActionException {
        if (!rc.canFireSingleShot()) return;
        if (!rc.canFireTriadShot()) {
            rc.fireSingleShot(dir);
        }
        else if (!rc.canFirePentadShot()) {
            if (rc.getTeamBullets() > RobotPlayer.BULLET_COUNT_MEDIUM) {
                rc.fireTriadShot(dir);
            }
            else {
                rc.fireSingleShot(dir);
            }
        }
        else if (rc.getTeamBullets() > RobotPlayer.BULLET_COUNT_HIGH) {
            rc.firePentadShot(dir);
        }
        else if (rc.getTeamBullets() > RobotPlayer.BULLET_COUNT_MEDIUM) {
            rc.fireTriadShot(dir);
        }
        else {
            rc.fireSingleShot(dir);
        }
    }
    void startLoop() throws GameActionException {
        info = true;
        sortedNearbyRobots = getSortedNearbyRobots();
    }
    void endLoop() throws GameActionException {
        if (retreat) {
            send(supposedNearestRobot);
        }
        else {
            if (nearbyVisibleEnemyRobots.length > 0) {
                if (nearbyVisibleEnemyRobots[0].ID % 2048 != supposedNearestRobot.ID) {
                    send(sortedNearbyRobots[0]);
                }
            }
        }
        shakeTrees();
        if (shouldWander) {
            move();
        }
        shakeTrees();
        Clock.yield();
    }
    float rad(double degrees) {
        return (float)(degrees * Math.PI / 180.0);
    }

}