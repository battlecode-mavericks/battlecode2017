package Matthew1;

import battlecode.common.*;

import java.util.Random;

public class Lumberjack extends Robot {
    Lumberjack(RobotController r) {
        super(r);
        while (true) {
            try {
                startLoop();
                everyLoop();
                endLoop();
            } catch (GameActionException g) {
                System.out.println("I had an error!  " + g);
                rc.setIndicatorDot(rc.getLocation(), 255, 255, 0);
                Clock.yield();
            }
        }
    }
    void everyLoop() throws GameActionException {
        // This is the stuff that happens every round
        avoidBullets();
        TreeInfo[] trees = rc.senseNearbyTrees();
        if (trees.length > 0) {
            shouldWander = false;
            if (rc.canChop(trees[0].ID)) {
                rc.chop(trees[0].ID);
            }
            else {
                tryMove(rc.getLocation().directionTo(trees[0].location), 90, 10);
            }
        }
        else {
            shouldWander = true;
        }
        if (rc.canStrike()) {
            if (rc.senseNearbyRobots(-1, rc.getTeam()).length <= rc.senseNearbyRobots(-1, rc.getTeam().opponent()).length) { // If there are less goodies than baddies then strike
                rc.strike();
            }
        }
        if (rc.getRobotCount() > 100 && rc.getTreeCount() == 0) rc.disintegrate();
    }
}