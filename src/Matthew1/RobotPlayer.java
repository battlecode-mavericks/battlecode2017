package Matthew1;

import battlecode.common.*;

import java.util.Random;

public class RobotPlayer {
    static MapLocation[] ourArchons;
    static MapLocation[] theirArchons;
    static MapLocation mapCenter;
    static Random random;
    public static final double BULLET_COUNT_HIGH = 300; // Anything above that is high
    public static final double BULLET_COUNT_MEDIUM = 100; // Anything between that and HIGH is medium
    // Anything else is low
    public static final double FIRE_RANGE_SOLDIER = 10; // Anything less than this and a soldier will shoot
    public static final double FIRE_RANGE_TANK = 5;
    public static final double FIRE_RANGE_SCOUT = 8;
    public static void run(RobotController r) throws GameActionException {
        random = new Random(r.getID());
        getMapCenter(r);
        switch (r.getType()) {
            case ARCHON:
                new Archon(r);
                break;
            case GARDENER:
                new Gardener(r);
                break;
            case LUMBERJACK:
                new Lumberjack(r);
                break;
            case SCOUT:
                new Scout(r);
                break;
            case SOLDIER:
                new Soldier(r);
                break;
            case TANK:
                new Tank(r);
                break;
        }
    }
    private static void getMapCenter(RobotController r) {
        ourArchons = r.getInitialArchonLocations(r.getTeam());
        theirArchons = r.getInitialArchonLocations(r.getTeam().opponent());
        float x = 0;
        float y = 0;
        for (int i = 0; i < ourArchons.length; i++) {
            x += ourArchons[i].x + theirArchons[i].x;
            y += ourArchons[i].y + theirArchons[i].y;
        }
        x /= ourArchons.length * 2;
        y /= ourArchons.length * 2;
        mapCenter = new MapLocation(x, y);
    }

}