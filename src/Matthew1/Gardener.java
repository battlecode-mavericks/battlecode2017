package Matthew1;

import battlecode.common.*;

import java.util.Random;

public class Gardener extends Robot {
    RobotType nextRobot = RobotType.SOLDIER;
    int roundAssigned;
    Gardener(RobotController r) {
        super(r);
        roundAssigned = rc.getRoundNum();
        while (true) {
            try {
                startLoop();
                everyLoop();
                endLoop();
            } catch (GameActionException g) {
                System.out.println("I had an error!  " + g);
                rc.setIndicatorDot(rc.getLocation(), 255, 255, 0);
                Clock.yield();
            }
        }
    }
    void everyLoop() throws GameActionException {
        // This is the stuff that happens every round

        if (nearbyVisibleEnemyRobots.length > 0) {
            avoidEnemies();
        }
        avoidBullets();
        Direction startDir;
        if (sortedNearbyRobots.length == 0) {
            startDir = new Direction(rad(RobotPlayer.random.nextInt(360)));
        } else {
            startDir = sortedNearbyRobots[0].location.directionTo(rc.getLocation());
        }
        if (rc.getTeamBullets() > nextRobot.bulletCost) {
            for (int i = 0; i < 360; i += 15) {
                if (rc.canBuildRobot(nextRobot, startDir.rotateLeftDegrees(i))) {
                    rc.buildRobot(nextRobot, startDir.rotateLeftDegrees(i));
                    generateNextRobot();
                    break;
                }
            }
        }
        if (roundAssigned - rc.getRoundNum() > RobotPlayer.random.nextInt(200) + 30) {
            generateNextRobot();
        }
    }
    void generateNextRobot() {
        double randomChances = RobotPlayer.random.nextDouble() * 100;
        System.out.println("c: " + randomChances);
        double lumberC = 15;
        double scoutC = 15;
        double tankC = 15;
        final double rC = rc.getRobotCount();
        final double rN = rc.getRoundNum();
        final double tC = rc.senseNearbyTrees().length;
        /*final*/ double lengthWOS = 0;
        for (RobotInfo robot : sortedNearbyRobots) {
            switch (robot.type) {
                case ARCHON:
                    lengthWOS += 10;
                    break;
                case TANK:
                    lengthWOS += 3;
                    break;
                case SCOUT:
                    lengthWOS += 1;
                    break;
                case LUMBERJACK:
                    lengthWOS += 0.75;
                    break;
                case GARDENER:
                    lengthWOS += 1.25;
                    break;
                case SOLDIER:
                    lengthWOS += 1.5;
            }
        }
        System.out.println("rC=" + rC + ", rN=" + rN + ", tC=" + tC + ", lengthWOS=" + lengthWOS);

        // Logic for changing chances:
        if (lengthWOS == 0) {
            lumberC += 10;
            scoutC += 15;
            tankC -= 10;
        }
        if (lengthWOS > 0) {
            lumberC += 8;
            scoutC -= 4;
            tankC += 4;
        }
        if (lengthWOS > 1) {
            lumberC -= 4;
            scoutC -= 4;
            tankC -= 4;
        }
        if (lengthWOS > 5) {
            lumberC -= 4;
            scoutC -= 4;
        }
        if (rC > 40) {
            lumberC -= 100;
            tankC += 10;
        }
        else if (rC > 30) {
            lumberC -= 10;
            tankC += 10;
        }
        else if (rC > 12) {
            lumberC -= 5;
            tankC += 5;
        }
        else if (rC > 4 && lengthWOS == 0) {
            lumberC += 20;
            scoutC += 10;
            tankC -= 100;
        }
        if (rN > 2000) {
            lumberC -= 10;
            scoutC -= 10;
        }
        if (tC > 8) {
            lumberC += 40;
            scoutC += 10;
            tankC -= 100;
        }
        else if (tC > 5) {
            lumberC += 30;
            scoutC += 7.5;
            tankC *= 0.1;
        }
        else if (tC > 2) {
            lumberC += 25;
            scoutC += 5;
            tankC *= 0.5;
        }
        else if (tC > 1) {
            lumberC += 12.5;
            scoutC += 3;
        }
        else if (tC > 0) {
            scoutC -= 2;
        }
        else if (tC == 0) {
            lumberC -= 3;
            scoutC -= 3;
            tankC += 3.75;
        }


        System.out.println("Odds: " + lumberC + "% lumber, " + scoutC + "% scout, " + tankC + "% tank.");
        if (lumberC < 0) lumberC = 0;
        if (scoutC < 0) scoutC = 0;
        if (tankC < 0) tankC = 0;

        if (randomChances < lumberC) {
            nextRobot = RobotType.LUMBERJACK; // 15%
        }
        else if (randomChances < lumberC + scoutC) {
            nextRobot = RobotType.SCOUT; // 15%
        }
        else if (randomChances < lumberC + scoutC + tankC) {
            nextRobot = RobotType.TANK; // 15%
        }
        else {
            nextRobot = RobotType.SOLDIER; // 55%
        }
        roundAssigned = rc.getRoundNum();
    }
}