package Matthew1;

import battlecode.common.*;

import java.util.Random;

public class Scout extends Robot {
    Scout(RobotController r) {
        super(r);
        while (true) {
            try {
                startLoop();
                everyLoop();
                endLoop();
            } catch (GameActionException g) {
                System.out.println("I had an error!  " + g);
                rc.setIndicatorDot(rc.getLocation(), 255, 255, 0);
                Clock.yield();
            }
        }
    }
    void everyLoop() throws GameActionException {
        // This is the stuff that happens every round
        avoidBullets();
        if (nearbyVisibleEnemyRobots.length > 0 ) {
            if (rc.getLocation().distanceTo(nearbyVisibleEnemyRobots[0].location) < RobotPlayer.FIRE_RANGE_SCOUT && rc.canFireSingleShot() && rc.getTeamBullets() > RobotPlayer.BULLET_COUNT_HIGH) {
                shoot(rc.getLocation().directionTo(nearbyVisibleEnemyRobots[0].location));
            }
        }
    }
}