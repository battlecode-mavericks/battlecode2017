package Matthew1;

import battlecode.common.*;

import java.util.List;

public class Archon extends Robot{
    Archon(RobotController r) {
        super(r);
        shouldWander = false;
        try {
            makeGardener();
        }
        catch (GameActionException e) {
            System.out.println("Couldn't make gardener on first turn...");
            rc.setIndicatorDot(rc.getLocation(), 255, 255, 0);
        }
        while (true) {
            try {
                startLoop();
                everyLoop();
                endLoop();
            }
            catch (GameActionException g) {
                System.out.println("I had an error!  " + g);
                rc.setIndicatorDot(rc.getLocation(), 255, 255, 0);
                Clock.yield();
            }
        }
    }
    void everyLoop() throws GameActionException {
        // This is the stuff that happens every round
        avoidEnemies();
        avoidBullets();
        if (RobotPlayer.random.nextInt((int)(rc.getRobotCount() * 1.5)) == 0) {
            makeGardener();
        }


        //if (rc.canHireGardener(new Direction(0))) {
         //   rc.hireGardener(new Direction(0));
        //}
    }
    boolean makeGardener() throws GameActionException {
        if (rc.getTeamBullets() >= RobotType.GARDENER.bulletCost) {
            Direction startDir = new Direction(0);
            for (int i = 0; i < 360; i += 10) {
                if (rc.canHireGardener(startDir.rotateLeftDegrees(i))) {
                    rc.hireGardener(startDir.rotateLeftDegrees(i));
                    return true;
                }
            }
        }
        return false;
    }
}