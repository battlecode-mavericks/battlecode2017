package ishanboot;
import battlecode.common.*;

/**
 * created by ishan kamat
 **/

public strictfp class RobotPlayer {
    static RobotController rc;
    static MapLocation[] initialArchonLocations;

    /**
     * run() is the method that is called when a robot is instantiated in the Battlecode world.
     * If this method returns, the robot dies!
     **/
    @SuppressWarnings("unused")
    public static void run(RobotController rc) throws GameActionException {

        // This is the RobotController object. You use it to perform actions from this robot,
        // and to get information on its current status.
        RobotPlayer.rc = rc;
        RobotPlayer.initialArchonLocations = rc.getInitialArchonLocations(rc.getTeam());

        // Here, we've separated the controls into a different method for each RobotType.
        // You can add the missing ones or rewrite this into your own control structure.
        switch (rc.getType()) {
            case ARCHON:
                runArchon();
                break;
            case GARDENER:
                runGardener();
                break;
            case SOLDIER:
                runSoldier();
                break;
            case LUMBERJACK:
                runLumberjack();
                break;
            case TANK:
                runTank();
                break;
            case SCOUT:
                runScout();
                break;
        }
    }

    static void runArchon() throws GameActionException {
        System.out.println("I'm an archon!");
        // Generate random direction
        // This will be changed whenever necessary
        Direction dir = randomDirection();
        int gardeners = 0;

        // The code you want your robot to perform every round should be in this loop
        while (true) {

            // Try/catch blocks stop unhandled exceptions, which cause your robot to explode
            try {
                // Randomly attempt to build a gardener in this direction
                if (rc.canHireGardener(dir) && (Math.random() < .01
                            || gardeners == 0)) {
                    rc.hireGardener(dir);
                    gardeners++;
                }

                // Move randomly
                if (!rc.canMove(dir)) {
                    dir = dir.rotateLeftDegrees(30);
                }
                tryMove(dir);
                MapLocation myLocation = rc.getLocation();

                BulletInfo[] bullets = rc.senseNearbyBullets();
                for (BulletInfo bullet : bullets) {
                    if (willCollideWithMe(bullet)) {
                        dir = myLocation.directionTo(bullet.location).opposite();
                        break;
                    }
                }

                // Broadcast archon's location for other robots on the team to know
                rc.broadcast(0,(int)myLocation.x);
                rc.broadcast(1,(int)myLocation.y);

                // Clock.yield() makes the robot wait until the next turn, then it will perform this loop again
                Clock.yield();

            } catch (Exception e) {
                System.out.println("Archon Exception");
                e.printStackTrace();
            }
        }
    }

    static void runGardener() throws GameActionException {
        System.out.println("I'm a gardener!");
        // Generate a random direction
        Direction moveDir = randomDirection();
        Direction buildDir = moveDir.opposite();
        float scouts = 0;
        float soldiers = 0;
        float lumberjacks = 0;
        int steps = 1;
        float randomN = (float) 0.92;

        // The code you want your robot to perform every round should be in this loop
        while (true) {

            // Try/catch blocks stop unhandled exceptions, which cause your robot to explode
            try {

                // Listen for home archon's location
                int xPos = rc.readBroadcast(0);
                int yPos = rc.readBroadcast(1);
                MapLocation archonLoc = new MapLocation(xPos,yPos);
                MapLocation gardenerLoc = rc.getLocation();


                // Randomly attempt to build a soldier or lumberjack in this direction
                if (!rc.canBuildRobot(RobotType.SOLDIER, buildDir) || !rc.canBuildRobot(RobotType.LUMBERJACK, buildDir)) {
                    buildDir = buildDir.rotateLeftDegrees((float)Math.random() * 30 - 15);
                }

                // generate a number for the entire if-else so that something has to be made if possible
                if (steps == 0) randomN = (float) Math.random();

                Direction dir = buildDir;

                if (rc.canBuildRobot(RobotType.SOLDIER, dir) && randomN < 0.6) {
                    // || Math.random() > (soldiers + 5) / 10)) {
                    System.out.println("Building a soldier...");
                    rc.buildRobot(RobotType.SOLDIER, dir);
                    soldiers++;
                    steps = -1;
                } else if (rc.canBuildRobot(RobotType.LUMBERJACK, dir) && 0.9 > randomN && randomN >= 0.6) {
                        //|| Math.random() > (lumberjacks + 5 / 10))) {
                    System.out.println("Building a lumberjack...");
                    rc.buildRobot(RobotType.LUMBERJACK, dir);
                    lumberjacks++;
                    steps = -1;
                } else if (rc.canBuildRobot(RobotType.SCOUT, dir) && 0.95 > randomN && randomN >= 0.9)  {
                    System.out.println("Building a scout...");
                    rc.buildRobot(RobotType.SCOUT, dir);
                    scouts++;
                    steps = -1;
                } else if (rc.canBuildRobot(RobotType.TANK, dir) && randomN >= 0.95) {
                    System.out.println("Building a tank...");
                    rc.buildRobot(RobotType.TANK, dir);
                    steps = -1;
                }
                // water trees if we can
                TreeInfo[] trees = new TreeInfo[3];
                trees = rc.senseNearbyTrees(RobotType.GARDENER.strideRadius);
                if (trees != null && trees.length > 0) {
                    if (rc.canShake() && trees.length > 0) {
                        rc.shake(trees[0].ID);
                        if (rc.canWater()) {
                            rc.water(trees[0].ID);
                        }
                    }
                }

                dir = moveDir;

                /*
                if (gardenerLoc.distanceTo(archonLoc) > RobotType.GARDENER.strideRadius * 5) {
                    dir = gardenerLoc.directionTo(archonLoc).rotateLeftDegrees(15);
                }
                */

                if (!rc.canMove(dir)) {
                    dir = dir.rotateLeftDegrees(60 * (float)Math.random() - 30);
                }
                if (rc.canMove(dir)) {
                    rc.move(dir);
                }
                moveDir = dir;
                buildDir = moveDir.opposite();
                steps = (steps + 1) % 30;

                // Clock.yield() makes the robot wait until the next turn, then it will perform this loop again
                Clock.yield();

            } catch (Exception e) {
                System.out.println("Gardener Exception");
                e.printStackTrace();
            }
        }
    }

    static void combatUnit() throws GameActionException {
        Team enemy = rc.getTeam().opponent();
        Direction dir = randomDirection();
        boolean doMove = true;
        // The code you want your robot to perform every round should be in this loop
        while (true) {

            // Try/catch blocks stop unhandled exceptions, which cause your robot to explode
            try {
                doMove = true;
                MapLocation myLocation = rc.getLocation();

                // See if there are any nearby enemy robots
                RobotInfo[] robots = rc.senseNearbyRobots(-1);

                // If there are some...
                if (robots.length > 0 && robots[0].team == enemy) {
                    // And we have enough bullets, and haven't attacked yet this turn...
                    if (rc.canFireSingleShot()) {
                        // ...Then fire a bullet in the direction of the enemy.
                        rc.fireSingleShot(myLocation.directionTo(robots[0].location));
                    }
                    dir = myLocation.directionTo(robots[0].location).rotateLeftDegrees((float)Math.random() * 130 - 75);
                } else {
                    // Move randomly
                    int x = rc.readBroadcast(2);
                    int y = rc.readBroadcast(3);
                    System.out.println("Recv robot: " + x + "x" + y);
                    if (x != 0 && y != 0) {
                        MapLocation target = new MapLocation(x, y);
                        if (myLocation.distanceTo(target) < rc.getType().sensorRadius && robots.length == 0) {
                            rc.broadcast(2, 0);
                            rc.broadcast(3, 0);
                        } else {
                            rc.setIndicatorDot(target, 0, 255, 0);
                            dir = myLocation.directionTo(target);
                            if (myLocation.distanceTo(target) < rc.getType().strideRadius * 2) {
                                doMove = false;
                            }
                        }
                    }
                    if (!rc.canMove(dir)) {
                        if (rc.canMove(dir.rotateLeftDegrees(60))) {
                            dir = dir.rotateLeftDegrees(60);
                        } else if (rc.canMove(dir.rotateLeftDegrees(-60))) {
                            dir = dir.rotateLeftDegrees(-60);
                        } else {
                            dir = randomDirection();
                        }
                    }
                    if (rc.canMove(dir) && doMove) {
                        rc.move(dir);
                    }
                }

                // Clock.yield() makes the robot wait until the next turn, then it will perform this loop again
                Clock.yield();

            } catch (Exception e) {
                System.out.println("Soldier Exception");
                e.printStackTrace();
            }
        }
    }

    static void runSoldier() throws GameActionException {
        System.out.println("I'm an soldier!");
        combatUnit();
    }

    static void runTank() throws GameActionException {
        System.out.println("I'm an tank!");
        combatUnit();
    }

    static void runScout() throws GameActionException {
        Direction dir = randomDirection();
        Team enemy = rc.getTeam().opponent();

        while (true) {
            try {
                int xPos = rc.readBroadcast(0);
                int yPos = rc.readBroadcast(1);
                MapLocation archonLoc = new MapLocation(xPos,yPos);
                if (!rc.canMove(dir)) {
                    if (rc.canMove(dir.rotateLeftDegrees(100))) {
                        dir = dir.rotateLeftDegrees(100);
                    } else {
                        dir = dir.rotateLeftDegrees(-100);
                    }
                }
                if (rc.canMove(dir) && !rc.hasMoved()) {
                    rc.move(dir);
                }
                TreeInfo[] trees = rc.senseNearbyTrees(RobotType.SCOUT.strideRadius);
                if (trees != null && trees.length > 0) {
                    for (TreeInfo tree : trees) {
                        if (rc.canShake(tree.ID)) {
                            rc.shake(tree.ID);
                        }
                    }
                }
                if (trees.length > 0) {
                    System.out.println("Sending tree: " + trees[0].location.x + "x" + trees[0].location.y);
                    rc.broadcast(4, (int)trees[0].location.x);
                    rc.broadcast(5, (int)trees[0].location.y);
                }
                RobotInfo[] robots = rc.senseNearbyRobots(-1);
                float lowest = (float)9999;
                RobotInfo lrobot = new RobotInfo(0, enemy, RobotType.ARCHON, archonLoc, (float)1.2, 0, 0);
                boolean sensed = false;
                for (RobotInfo robot : robots) {
                    if (archonLoc.distanceTo(robot.location) < lowest && robot.team == enemy) {
                        lrobot = robot;
                        lowest = archonLoc.distanceTo(robot.location);
                        sensed = true;
                    }
                }
                if (sensed) {
                    System.out.println("Sending robot: " + lrobot.location.x + "x" + lrobot.location.y);
                    rc.broadcast(2, (int)lrobot.location.x);
                    rc.broadcast(3, (int)lrobot.location.y);
                }
                if (rc.canFireSingleShot() && robots.length > 0 && robots[0].team == enemy) {
                    if (Math.random() < 0.25) {
                        rc.fireSingleShot(rc.getLocation().directionTo(robots[0].location));
                    }
                    if (rc.getLocation().distanceTo(robots[0].location) < RobotType.SCOUT.sensorRadius * 0.75) {
                        dir = rc.getLocation().directionTo(robots[0].location).opposite();
                    } else {
                        dir = rc.getLocation().directionTo(robots[0].location);
                    }
                }
                Clock.yield();
            } catch (Exception e) {
                System.out.println("Scout Exception");
                e.printStackTrace();
            }
        }
    }

    static void runLumberjack() throws GameActionException {
        System.out.println("I'm a lumberjack!");
        Team enemy = rc.getTeam().opponent();
        Direction dir = randomDirection();
        boolean targetingDir = false;

        // The code you want your robot to perform every round should be in this loop
        while (true) {

            // Try/catch blocks stop unhandled exceptions, which cause your robot to explode
            try {

                // See if there are any enemy robots within striking range (distance 1 from lumberjack's radius)
                RobotInfo[] robots = rc.senseNearbyRobots(RobotType.LUMBERJACK.bodyRadius
                        + GameConstants.LUMBERJACK_STRIKE_RADIUS, enemy);
                TreeInfo[] trees = rc.senseNearbyTrees(RobotType.LUMBERJACK.bodyRadius
                        + RobotType.GARDENER.strideRadius);

                MapLocation treeLoc = new MapLocation(rc.readBroadcast(4), rc.readBroadcast(5));
                System.out.println("Recv tree: " + treeLoc.x + "x" + treeLoc.y);
                if (treeLoc.x != 0 && treeLoc.y != 0) {
                    rc.setIndicatorDot(treeLoc, 0, 0, 255);
                    MapLocation here = rc.getLocation();
                    if (here.distanceTo(treeLoc) < RobotType.LUMBERJACK.sensorRadius && trees.length == 0) {
                        rc.broadcast(4, 0);
                        rc.broadcast(5, 0);
                        targetingDir = false;
                    }
                    dir = here.directionTo(treeLoc);
                    targetingDir = true;
                } else {
                    System.out.println("Ignoring tree");
                }
                if (trees != null && trees.length > 0) {
                    //System.out.print("Trees found: ");
                    //System.out.println(trees.length);
                    for (TreeInfo tree : trees) {
                        if (rc.canChop(tree.ID)) {
                            System.out.println("Chopping a tree!");
                            rc.chop(tree.ID);
                        }
                    }
                    MapLocation here = rc.getLocation();
                    dir = here.directionTo(trees[0].getLocation());
                    targetingDir = true;
                } else if (robots.length > 0 && !rc.hasAttacked()) {
                    // Use strike() to hit all nearby robots!
                    rc.strike();
                }
                // No close robots, so search for robots within sight radius
                robots = rc.senseNearbyRobots(-1,enemy);

                if (targetingDir) {
                    if (!rc.canMove(dir)) {
                        targetingDir = false;
                    } else {
                        rc.move(dir);
                    }
                    if (Math.random() < 0.005) {
                        targetingDir = false;
                    }
                    dir = dir.rotateLeftDegrees(2);
                } else if (robots.length > 0) {
                    // If there is a robot, move towards it
                    MapLocation myLocation = rc.getLocation();
                    MapLocation enemyLocation = robots[0].getLocation();
                    tryMove(myLocation.directionTo(enemyLocation));
                    targetingDir = false;
                } else {
                    // Move Randomly
                    tryMove(randomDirection());
                    targetingDir = false;
                }

                // Clock.yield() makes the robot wait until the next turn, then it will perform this loop again
                Clock.yield();

            } catch (Exception e) {
                System.out.println("Lumberjack Exception");
                e.printStackTrace();
            }
        }
    }

    /**
     * Returns a random Direction
     * @return a random Direction
     */
    static Direction randomDirection() {
        return new Direction((float)Math.random() * 2 * (float)Math.PI);
    }

    /**
     * Attempts to move in a given direction, while avoiding small obstacles directly in the path.
     *
     * @param dir The intended direction of movement
     * @return true if a move was performed
     * @throws GameActionException
     */
    static boolean tryMove(Direction dir) throws GameActionException {
        return tryMove(dir,20,3);
    }

    /**
     * Attempts to move in a given direction, while avoiding small obstacles direction in the path.
     *
     * @param dir The intended direction of movement
     * @param degreeOffset Spacing between checked directions (degrees)
     * @param checksPerSide Number of extra directions checked on each side, if intended direction was unavailable
     * @return true if a move was performed
     * @throws GameActionException
     */
    static boolean tryMove(Direction dir, float degreeOffset, int checksPerSide) throws GameActionException {

        // First, try intended direction
        if (rc.canMove(dir)) {
            rc.move(dir);
            return true;
        }

        // Now try a bunch of similar angles
        boolean moved = false;
        int currentCheck = 1;

        while(currentCheck<=checksPerSide) {
            // Try the offset of the left side
            if(rc.canMove(dir.rotateLeftDegrees(degreeOffset*currentCheck))) {
                rc.move(dir.rotateLeftDegrees(degreeOffset*currentCheck));
                return true;
            }
            // Try the offset on the right side
            if(rc.canMove(dir.rotateRightDegrees(degreeOffset*currentCheck))) {
                rc.move(dir.rotateRightDegrees(degreeOffset*currentCheck));
                return true;
            }
            // No move performed, try slightly further
            currentCheck++;
        }

        // A move never happened, so return false.
        return false;
    }

    /**
     * A slightly more complicated example function, this returns true if the given bullet is on a collision
     * course with the current robot. Doesn't take into account objects between the bullet and this robot.
     *
     * @param bullet The bullet in question
     * @return True if the line of the bullet's path intersects with this robot's current position.
     */
    static boolean willCollideWithMe(BulletInfo bullet) {
        MapLocation myLocation = rc.getLocation();

        // Get relevant bullet information
        Direction propagationDirection = bullet.dir;
        MapLocation bulletLocation = bullet.location;

        // Calculate bullet relations to this robot
        Direction directionToRobot = bulletLocation.directionTo(myLocation);
        float distToRobot = bulletLocation.distanceTo(myLocation);
        float theta = propagationDirection.radiansBetween(directionToRobot);

        // If theta > 90 degrees, then the bullet is traveling away from us and we can break early
        if (Math.abs(theta) > Math.PI/2) {
            return false;
        }

        // distToRobot is our hypotenuse, theta is our angle, and we want to know this length of the opposite leg.
        // This is the distance of a line that goes from myLocation and intersects perpendicularly with propagationDirection.
        // This corresponds to the smallest radius circle centered at our location that would intersect with the
        // line that is the path of the bullet.
        float perpendicularDist = (float)Math.abs(distToRobot * Math.sin(theta)); // soh cah toa :)

        return (perpendicularDist <= rc.getType().bodyRadius);
    }

    static void sendRobot(int channel, RobotInfo robot) throws GameActionException {
        int data = 0;
        int adjx = (int)(robot.location.x - initialArchonLocations[0].x) / 4;
        int adjy = (int)(robot.location.y - initialArchonLocations[0].y) / 4;
        data += ((int)(adjx > 0 ? 1 : 0) << 30);
        data += ((int)(adjy > 0 ? 1 : 0) << 28);
        data += ((int)robot.type.ordinal() << 20);
        data += ((int)robot.ID << 12);
        data += (Math.abs(adjx) << 6);
        data += (Math.abs(adjy));
        System.out.println("Sent: " + (adjx + initialArchonLocations[0].x) + "x" + (adjy + initialArchonLocations[0].y));
        rc.broadcast(channel, data);
    }

    static boolean robotRecvReady(int channel) throws GameActionException {
        try {
            if (rc.readBroadcast(channel) != 0) {
                return true;
            }
        } catch (Exception e) {
        }
        return false;
    }

    static RobotInfo receiveRobot(int channel) throws GameActionException {
        int data = rc.readBroadcast(channel);
        int type, health, ID, x, xpos, y, ypos;
        xpos = data >> 30;
        data -= (data >> 30) << 30;
        ypos = data >> 28;
        data -= (data >> 28) << 28;
        type = data >> 20;
        data -= (data >> 20) << 20;
        ID = data >> 12;
        data -= (data >> 12) << 12;
        x = ((xpos == 1 ? 1 : -1) * (data >> 6)) + (int)initialArchonLocations[0].x/4;
        x *= 4;
        data -= (data >> 6) << 6;
        y = ((ypos == 1 ? 1 : -1) * data) + (int)initialArchonLocations[0].y/4;
        y *= 4;
        MapLocation loc = new MapLocation(x, y);
        RobotInfo robot = new RobotInfo(ID, rc.getTeam().opponent(), RobotType.values()[0], loc,
                (float)100, 0, 0);
        System.out.println("Recv: " + x + "x" + y);
        return robot;
    }
}
